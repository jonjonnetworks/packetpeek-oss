# Packetpeek OSS Edition `*beta*`

[![coverage report](https://gitlab.com/jonjonnetworks/packetpeek-oss/badges/dev/coverage.svg)](https://gitlab.com/jonjonnetworks/packetpeek-oss/-/commits/dev)

Welcome to Packetpeek Open Source Software edition!

Check the [Demo!](https://packetpeek.jonahstfrancois.com)

## What is Packetpeek?

Packetpeek is a modular, lightweight, and hackable IoT (Internet of Things) platform written in GoLang. It contains a base package, which handles non device-specific functionality, as well as a BIOT package that adds on BIOT device support. The platform is open source, and designed as a great starting point for IoT experimentation at home, while also being robust enough to stand as a commercial product.

## Copyright and Software License

By using Packetpeek, you agree to comply with the Software License (**to be included as**) "LICENSE". In layman's terms, it's free for personal use, can be used in open source and non-profit applications with attribution, and can _not_ be used commercially without prior written agreement and attribution.

## Installation

Installation is very easy. Simply download and unzip the appropriate package, Packetpeek will automatically create the required database files in the same directory. Once Packetpeek is running, open your preferred browser and navigate to http://localhost:8081 and bookmark it with the name "Packetpeek" for easy access.

## Downloads

Downloads have been moved to the [releases](https://gitlab.com/jonjonnetworks/packetpeek-oss/-/releases) page.

## Settings

### `Server Settings`

Server settings can be found in the menu in top right corner of the dashboard. In this section, you can find:

- `Your name`, for in alert emails
- `Your email`, for sending alerts to
- `Timezone`, for correctly formatting dashboard elements (in [TZ format](https://timezonedb.com/time-zones))
- `Units`, Metric or Imperial, for Celsius/Fahrenheit formatting
- `Graph Data Points`, for the number of records to display in the graphs
- An SMTP section, for using your email provider, with the fields:
  - `Username`, the email/username used for logging in
  - `**Password`, the password used for logging in
  - `Host`, the SMTP host (example smtp.gmail.com)
  - `Port`, the SMTP port, 587 is one standard
  - `Alerts`, a checkbox enabling Packetpeek to send you alert emails
- Server Ports
  - `Port`, the dashboard port number (takes effect after server restart)
  - `BIOT Port`, the port number BIOTs will send data to (must be changed on all BIOTs as well)
  - `Port Range Limiter`, a checkbox enabling limiting ports to 1024-65,535 (diasble with caution)

`**`Gmail makes things a little tricky to set up SMTP, as you can't directly use your regular email password. This is a Google security feature, and can't be changed. To use a Gmail account, you just need to add an App Key to your account. [Google's Simple Instructions](https://support.google.com/accounts/answer/185833?hl=en)

### `BIOT Settings`

BIOT settings can be found in the top right corner of the box containing its nickname and ID. In this section, you can find:

- `Wake Interval Hours`, for how many hours the device will sleep between messages (longer = exponentially better battery life)
- `Wake Interval Minutes`, for how many minutes the device will sleep between messages (longer = better battery life)
- `Melody`, if you want the device to chime when it sends messages
- `Nickname`,for giving the device a recognizable nickname
- `Door Alerts`, if the device is installed on a door frame you can set it to notify you when:
  - Never
  - When door opens
  - When door closes
  - When door left open
  - When door left closed
- `Flood Alert Interval`, for how many minutes the device will wait to repeat flood alerts

## Bugs, Issues, Questions?

[Let me know!](mailto:jonah.tech11@gmail.com)
