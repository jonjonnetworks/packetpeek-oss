package base

import (
	"bytes"
	"encoding/json"

	// "log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jonjonnetworks/packetpeek-oss/tools"

	log "github.com/sirupsen/logrus"
)

const testBaseSettingsTable = "test_settings.db"

func TestSettingsBootStrap(t *testing.T) {
	assert := assert.New(t)

	settings, err := SettingsBootStrap(testBaseSettingsTable)
	assert.NoError(err)
	assert.NotNil(settings.Port)

	// Bootstrapping subsequently should skip importing with logged message
	str := captureOutput(func() { SettingsBootStrap(testBaseSettingsTable) })
	assert.NotNil(str)

	assert.NoError(tools.DeleteTestDatabases(testBaseSettingsTable))
}

func captureOutput(f func()) string {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	f()
	log.SetOutput(os.Stderr)
	return buf.String()
}

func TestSettingsInit(t *testing.T) {
	assert := assert.New(t)
	SettingsInit(testBaseSettingsTable)
	assert.NoError(SettingsImport())
	assert.NoError(tools.DeleteTestDatabases(testBaseSettingsTable))
}

func TestSettingsInitSkipsIfExist(t *testing.T) {
	assert := assert.New(t)
	SettingsInit(testBaseSettingsTable)
	assert.NoError(SettingsImport())
	assert.Error(SettingsImport())
	assert.NoError(tools.DeleteTestDatabases(testBaseSettingsTable))
}

func TestSettingRead(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(Routes())
	SettingsInit(testBaseSettingsTable)
	SettingsImport()

	req, err := http.Get(httpServer.URL + "/settings")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal(true, resp["LimitPorts"])

	assert.NoError(tools.DeleteTestDatabases(testBaseSettingsTable))
}

func TestSettingUpdate(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(Routes())
	SettingsInit(testBaseSettingsTable)
	SettingsImport()

	set := map[string]interface{}{"LimitPorts": false}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	req, err := http.Get(httpServer.URL + "/settings")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	response := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&response)
	assert.NoError(err)
	assert.Equal(false, response["LimitPorts"])

	assert.NoError(tools.DeleteTestDatabases(testBaseSettingsTable))
}

func TestSettingUpdateFailsOnNoRowsAffected(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(Routes())
	SettingsInit(testBaseSettingsTable)
	SettingsImport()

	set := map[string]interface{}{"oranges": false}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	assert.NoError(tools.DeleteTestDatabases(testBaseSettingsTable))
}

func TestSettingUpdateFailsOnBadInput(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(Routes())

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, resp.StatusCode)
}
