package base

import (
	"net/http"

	"github.com/gorilla/mux"
)

var RootRedirect string

// Router creates our Mux routing
func Routes() *mux.Router {
	mux := mux.NewRouter().StrictSlash(true)

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, RootRedirect, http.StatusMovedPermanently)
	})

	mux.HandleFunc("/ping", ping).Methods("GET")

	mux.HandleFunc("/settings", SettingsRead).Methods("GET")
	mux.HandleFunc("/settings", SettingUpdate).Methods("PATCH")

	return mux
}

// Ping is for programmatic polling - replies pong!
func ping(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{ "message": "pong!" }`))
}

func SetRootRedirect(redirect string) {
	RootRedirect = redirect
}
