package base

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

// ROUTER

func TestRouter(t *testing.T) {
	assert := assert.New(t)
	r := Routes()
	assert.NotNil(r)

	original := "/ping"
	SetRootRedirect(original)

	assert.Equal(original, RootRedirect)

	httpServer := httptest.NewServer(r)
	req, err := http.Get(httpServer.URL)
	assert.NoError(err)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("pong!", resp["message"])
}
