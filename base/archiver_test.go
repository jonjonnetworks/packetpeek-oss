package base

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEnableArchiving(t *testing.T) {
	assert := assert.New(t)

	assert.False(ArchiveEnabled)
	assert.Equal("dailyavg", ArchiveMethod)
	EnableArchiving("weeklyavg")
	assert.True(ArchiveEnabled)
	assert.Equal("weeklyavg", ArchiveMethod)
}

func TestPrepareTXHandeler(t *testing.T) {
	assert := assert.New(t)

	a := Archiver{
		SRCHandler:    nil,
		DSTHandler:    nil,
		LeaveXRecords: 0,
	}
	assert.Equal(a, a)

	// dailyAvg(a)
}
