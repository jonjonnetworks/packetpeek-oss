package base

import (
	"github.com/alexbyk/panicif"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var SettingsDB *gorm.DB

func SettingsInit(dbname string) {
	// Create Base Settngs DB
	var err error
	SettingsDB, err = gorm.Open("sqlite3", dbname)
	panicif.Err(err)
	SettingsDB.DB().SetMaxIdleConns(20)
	SettingsDB.DB().SetMaxOpenConns(200)
	SettingsDB.AutoMigrate(&Settings{})
}
