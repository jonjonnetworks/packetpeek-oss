package base

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

/*
SUPPORTED ARCHIVING METHODS, into "archive_[src database name].db"
	dailyavg = Takes the average of all data points per day, and inserts one averaged record
	weeklyavg = Takes the average of all data points per week and inserts one averaged record
	singlerecordavg = Takes all the current data, and averages it into one record (good for scheduled flexibility)
 	snapshotavg = Takes an average of data batched by `graphPoints` into one record
*/

type Archiver struct {
	SRCHandler    *gorm.DB
	DSTHandler    *gorm.DB
	LeaveXRecords int
}

var ArchiveEnabled = false
var ArchiveMethod = "dailyavg"

func EnableArchiving(method string) {
	ArchiveEnabled = true
	ArchiveMethod = method
}

// Archive will take a specified archiving strategy
// and apply it to the provided database handler
func Archive(a Archiver) (int, error) {
	switch ArchiveMethod {
	case "dailyavg":
		return dailyAvg(a)
	case "weeklyavg":
		return weeklyAvg(a)
	case "singlerecordavg":
		return singleRecordAvg(a)
	case "snapshotavg":
		return snapshotAvg(a)
	}

	return 0, fmt.Errorf("unsupported archiving method " + ArchiveMethod)
}

// Prepares the archive database handler
func prepareTXHandler(a Archiver) (*gorm.DB, error) {
	tx := a.DSTHandler.Begin()

	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		return nil, err
	}

	return tx, nil
}

var count, offset int

func dailyAvg(a Archiver) (int, error) {
	tx, err := prepareTXHandler(a)
	if err != nil {
		return 0, err
	}

	var data map[string]interface{}
	a.SRCHandler.Limit(10).Offset(5).Find(&data)
	fmt.Println(data)
	fmt.Println(tx.Error)

	// Recursive
	// a.SRCHandler.Find().fromBeginning.Limit(10).offset(0).
	// avg := (float64(sum)) / (float64(n))
	// tx.Create().Batch()

	// res := tx.Commit()
	// return int(res.RowsAffected), res.Error
	return 0, nil
}
func weeklyAvg(a Archiver) (int, error) {
	return 0, nil
}
func singleRecordAvg(a Archiver) (int, error) {
	return 0, nil
}
func snapshotAvg(a Archiver) (int, error) {
	return 0, nil
}
