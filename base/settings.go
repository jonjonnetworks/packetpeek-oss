package base

import (
	"encoding/json"
	"fmt"

	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/jinzhu/gorm"
)

// Settings holds our dashboard settings
type Settings struct {
	gorm.Model
	Name       string
	Email      string
	Timezone   string
	Units      string
	Port       int
	LimitPorts bool
	SMTPUser   string
	SMTPPass   string
	SMTPHost   string
	SMTPPort   int
	Alerts     bool
}

func SettingsBootStrap(settingsdb string) (Settings, error) {
	SettingsInit(settingsdb)
	if err := SettingsImport(); err != nil {
		if err.Error() != "skip" {
			return Settings{}, err
		}
	}

	var s Settings
	SettingsDB.First(&s)
	return s, nil
}

func SettingsImport() error {
	// Make sure there are no existing settings
	var s Settings
	SettingsDB.Find(&s)
	if s.Name != "" && s.Email != "" && s.Timezone != "" && s.Units != "" && s.Port != 0 {
		if s.SMTPUser == "example@example.com" ||
			s.SMTPPass == "examplepassword" ||
			s.SMTPHost == "smtp.example.com" ||
			s.SMTPPort == 0 ||
			s.Email == "example@example.com" {
			log.Warn("Default SMTP settings detected, alerts will be disabled until credentials are added")
		}

		return fmt.Errorf("skip")
	}

	log.Warn("Missing SMTP credentials, email alerts will be disabled until credentials are added")
	defaultSettings := Settings{
		Name:       "Packetpeek User",
		Email:      "example@example.com",
		Timezone:   "America/Toronto",
		Units:      "Metric",
		Port:       8081,
		LimitPorts: true,
		Alerts:     false,
		SMTPUser:   "example@example.com",
		SMTPPass:   "examplepassword",
		SMTPHost:   "smtp.example.com",
		SMTPPort:   587,
	}

	return SettingsDB.Create(&defaultSettings).Error
}

// GetSettings returns a settings object
func GetSettings() (s Settings) {
	SettingsDB.Find(&s)
	return
}

// SettingsRead will return server settings
func SettingsRead(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(GetSettings())
}

// SettingUpdate will update server settings
func SettingUpdate(w http.ResponseWriter, r *http.Request) {
	var settingsUpdate map[string]interface{} // Using map so we can update the boolean field to false
	if err := json.NewDecoder(r.Body).Decode(&settingsUpdate); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{ "message": "Error Unmarshaling JSON" }`))
		return
	}

	var settings Settings
	if SettingsDB.First(&settings).Update(settingsUpdate).RowsAffected < 1 {
		respJSON := []byte(`{ "message": "Settings not found!" }`)
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	w.WriteHeader(http.StatusOK)
}
