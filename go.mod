module gitlab.com/jonjonnetworks/packetpeek-oss

go 1.16

require (
	github.com/alexbyk/panicif v1.1.0
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876 // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
)
