#!/bin/sh

cov=$(go test -cover -coverprofile=code.cov ./... && go tool cover -func code.cov && rm code.cov)
covered=$(echo "$cov" | grep "(statements)" | awk '{print $NF+0}')

great=90.0
pass=80.0

green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
red="$(tput setaf 1)"

reset="$(tput sgr0)"

color=$red

if awk "BEGIN {exit !($covered >= $great)}"; then 
    color=$green
elif awk "BEGIN {exit !($covered >= $pass)}"; then  
    color=$yellow 
fi

printf "%s%s%% coverage%s\n" $color $covered $reset