package tools

import (
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// Putting it here so it's accessible from other tests
func DeleteTestDB() error {
	if err := os.Remove("./test.db"); err != nil {
		return err
	}
	return nil
}

func TestPadString(t *testing.T) {
	assert := assert.New(t)

	stringToPad := "TestString"
	padded := PadString(stringToPad, "+", 12)
	assert.NotNil(padded)

	expected := "TestString++"
	assert.Equal(expected, padded)
}

func TestPadNum(t *testing.T) {
	assert := assert.New(t)

	numToPad := 2
	padded := PadNum(numToPad)
	assert.NotNil(padded)

	expected := "02"
	assert.Equal(expected, padded)
}

func TestRandSeq(t *testing.T) {
	assert := assert.New(t)

	lengthToGenerate := 6
	generatedString := RandSeq(lengthToGenerate)
	assert.NotNil(generatedString)
	assert.Len(generatedString, 6)
}

func TestMakeNum(t *testing.T) {
	assert := assert.New(t)

	stringToMakeNum := "6"
	num := MakeNum(stringToMakeNum)
	assert.NotNil(num)

	expected := 6
	assert.Equal(expected, num)
}

func TestMakeNumReturnsZeroBadInput(t *testing.T) {
	assert := assert.New(t)

	stringToMakeNum := "asd"
	num := MakeNum(stringToMakeNum)

	expected := 0
	assert.Equal(expected, num)
}

func TestMakeFloat(t *testing.T) {
	assert := assert.New(t)

	stringToMakeFloat := "6.5"
	num := MakeFloat(stringToMakeFloat, 0.1)
	assert.NotNil(num)

	assert.Equal(6.5, num)
}

func TestMakeFloatReturnsZeroBadInput(t *testing.T) {
	assert := assert.New(t)

	stringToMakeFloat := "asd"
	num := MakeFloat(stringToMakeFloat, 0.1)

	assert.Equal(0.0, num)
}

func TestTrimFloat(t *testing.T) {
	assert := assert.New(t)
	assert.Equal("5.5", TrimFloatPointOne(5.5001))
}

func TestBattery2Percent(t *testing.T) {
	assert := assert.New(t)

	// Full battery
	millivolts := 3100
	percent := Battery2percent(millivolts)
	assert.NotNil(percent)

	expected := 100.00
	assert.Equal(expected, percent)

	// Used battery
	millivolts = 2995
	percent = Battery2percent(millivolts)
	assert.NotNil(percent)

	expected = 69
	assert.Equal(expected, percent)

	// Empty battery
	millivolts = 2600
	percent = Battery2percent(millivolts)
	assert.NotNil(percent)

	expected = 0
	assert.Equal(expected, percent)
}

func TestDK2CF(t *testing.T) {
	assert := assert.New(t)

	deciKelvin := 2905

	celsius, fahrenheit := Dk2cf(deciKelvin)
	assert.NotNil(celsius)
	assert.NotNil(fahrenheit)

	expectedC := 17.35
	expectedF := 63.23

	assert.Equal(expectedC, celsius)
	assert.Equal(expectedF, fahrenheit)
}

func TestResistance2Moisture(t *testing.T) {
	assert := assert.New(t)

	resistance := 240
	moisture := Resistance2moisture(resistance)
	assert.NotNil(moisture)

	expected := 80.00
	assert.Equal(expected, moisture)
}

func TestTranslateDoorStatus(t *testing.T) {
	assert := assert.New(t)

	assert.Equal("Wide Open", TranslateDoorStatus("q"))
	assert.Equal("Wide Open", TranslateDoorStatus("r"))
	assert.Equal("Closed Shut", TranslateDoorStatus("e"))
	assert.Equal("Closed Shut", TranslateDoorStatus("f"))
	assert.Equal("Recently Opened", TranslateDoorStatus("p"))
	assert.Equal("Recently Closed", TranslateDoorStatus("d"))
	assert.Equal("Unknown", TranslateDoorStatus("-"))
}

// // skipping because it takes 2s per test
// func skipTestHashPassword(t *testing.T) {
// 	assert := assert.New(t)

// 	password := "test"
// 	hash, err := HashPassword(password)
// 	assert.NoError(err)
// 	assert.NotNil(hash)

// 	// bcrypt generates a different hash every time it's called, we cant check literal match
// 	expected := "$2a$14$"
// 	runes := []rune(hash)
// 	assert.Equal(expected, string(runes[0:7]))

// 	expected2 := 52
// 	runes = []rune(hash)
// 	assert.Len(string(runes[8:]), expected2)
// }

// // skipping because it takes 2s per test
// func skipTestCheckPasswordHash(t *testing.T) {
// 	assert := assert.New(t)

// 	password := "test"
// 	hash := "$2a$14$zMMevvjrzI5E6vgGEQaPbuwZoyNgBtCQue5uGhYLmGxJUsP/MXVoO"
// 	match := CheckPasswordHash(password, hash)
// 	assert.NotNil(match)

// 	expected := true
// 	assert.Equal(expected, match)
// }
