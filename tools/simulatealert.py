from urllib.parse import urlencode
from urllib.request import Request, urlopen

url = 'http://localhost:8081/dev/f1'


# Sending 'p' for door as that is default for alert trigger
post_fields = {
    'id': 'a1:b2:c3:d4:e5:f6',
    'bat_lev': 2048,
    'temp': 4000,
    'hum': 4000,
    'door': 'p',
    'nick': 'Demo Device 1',
}
request = Request(url, urlencode(post_fields).encode())
val = urlopen(request).read()
print(val)
