from urllib.parse import urlencode
from urllib.request import Request, urlopen
import random


def d():
    options = ['p', 'd', 'q', 'e', 'r', 'f']
    return options[random.randint(0, 3)]


url = 'http://localhost:7875/dev/f1'
post_fields = {
    'id': 'aa:aa:aa:aa:aa:aa',
    'bat_lev': 2960,
    'temp': random.randint(2731, 3031),
    'hum': random.randint(195, 4096),
    'door': 'r',
    'nick': 'Simulated BIOT 1',
}

request = Request(url, urlencode(post_fields).encode())
print(urlopen(request).read())