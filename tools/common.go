package tools

import "os"

func DeleteTestDatabases(databases ...string) error {
	for _, d := range databases {
		if err := os.Remove(d); err != nil {
			return err
		}
	}
	return nil
}
