package tools

import (
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/stretchr/testify/assert"
)

var tDB *gorm.DB

func TestDeleteTestDatabases(t *testing.T) {
	assert := assert.New(t)

	type ts struct {
		gorm.Model
	}
	var e error
	tDB, e = gorm.Open("sqlite3", "test.db")
	assert.NoError(e)
	tDB.AutoMigrate(&ts{})
	assert.NoError(DeleteTestDatabases("test.db"))
	assert.Error(DeleteTestDatabases("test.db"))
}
