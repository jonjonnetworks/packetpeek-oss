package main

import (
	"fmt"
	"net/http"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	log "github.com/sirupsen/logrus"
	"gitlab.com/jonjonnetworks/packetpeek-oss/base"
	"gitlab.com/jonjonnetworks/packetpeek-oss/biot"
)

func init() {
	log.SetFormatter(&log.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
		FullTimestamp:   true,
	})
	// log.Warn("The SQlite3 errors above are normal, and come from upstream.")
	log.Info("Initiating server...")
}

func main() {
	// Set up the platform base
	// TODO: I dont want to pass in db names everywhere,
	// but tests leave dbs everywhere otherwise.
	// To avoid tests deleting live data, they must build and teardown their own
	baseSettings, err := base.SettingsBootStrap("settings.db")
	if err != nil {
		panic(err.Error())
	}

	// Start up our BIOT device server
	go biot.Start()

	// Collect our routes
	http.Handle("/", base.Routes())
	http.Handle("/biot/", biot.Routes())

	// Set root URL redirect
	base.SetRootRedirect("/biot/")

	// Enable data archiving
	// base.EnableArchiving("weeklyavg")

	// Start up our base server
	log.Infof("Starting webserver on port %d...\n", baseSettings.Port)
	err = http.ListenAndServe(fmt.Sprintf(":%d", baseSettings.Port), nil)
	if err != nil {
		panic(err)
	}
}
