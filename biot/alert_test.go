package biot

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jonjonnetworks/packetpeek-oss/base"
	"gitlab.com/jonjonnetworks/packetpeek-oss/tools"
)

func TestManageAlertsNilsOnNoOp(t *testing.T) {
	assert := assert.New(t)
	base.SettingsBootStrap("test_settings.db")

	assert.NoError(manageAlerts(Device{}, Data{}))

	tools.DeleteTestDatabases("test_settings.db")
}

func TestManageAlertsErrorsOnAssemblyError(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(base.Routes())
	base.SettingsBootStrap("test_settings.db")

	set := map[string]interface{}{"SMTPHost": "", "SMTPUser": "", "SMTPPass": "", "SMTPPort": 0}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	tDev := Device{DoorAlert: "q"}
	tDat := Data{HallSense: "q"}

	assert.Error(manageAlerts(tDev, tDat))

	tools.DeleteTestDatabases("test_settings.db")
}

func TestManageAlertsErrorsOnSendError(t *testing.T) {
	assert := assert.New(t)

	base.SettingsBootStrap("test_settings.db")

	tDev := Device{DoorAlert: "q"}
	tDat := Data{HallSense: "q"}

	assert.Error(manageAlerts(tDev, tDat))

	tools.DeleteTestDatabases("test_settings.db")
}

func TestCheckAlert(t *testing.T) {
	assert := assert.New(t)

	base.SettingsBootStrap("test_settings.db")

	var tDev Device
	var tDat Data
	tDat.HallSense = "r"

	a, r := checkAlert(tDev, tDat)
	assert.True(a)
	assert.Equal("flood", r)

	tDev.Mode = 2
	a, r = checkAlert(tDev, tDat)
	assert.True(a)
	assert.Equal("water", r)

	tDev.DoorAlert = "q"
	tDat.HallSense = "q"
	a, r = checkAlert(tDev, tDat)
	assert.True(a)
	assert.Equal("door", r)

	tools.DeleteTestDatabases("test_settings.db")
}

func TestAssembleAlertErrorsOnMissingCredentials(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(base.Routes())
	base.SettingsBootStrap("test_settings.db")

	set := map[string]interface{}{"SMTPHost": "smtp.example.com", "SMTPUser": "example@example.com", "SMTPPass": "examplepassword", "SMTPPort": 587}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	_, e := AssembleAlertEmail("", Device{Nickname: "test"})
	assert.Error(e)
	tools.DeleteTestDatabases("test_settings.db")
}

func TestAssembleAlertErrorsOnMissingInfo(t *testing.T) {
	assert := assert.New(t)
	base.SettingsBootStrap("test_settings.db")

	httpServer := httptest.NewServer(base.Routes())
	base.SettingsBootStrap("test_settings.db")

	set := map[string]interface{}{"SMTPHost": "smtp.example.com", "SMTPUser": "human@example.com", "SMTPPass": "sample", "SMTPPort": 587}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	a, e := AssembleAlertEmail("", Device{Nickname: "test"})
	assert.NoError(e)
	assert.NotNil(a)
	tools.DeleteTestDatabases("test_settings.db")
}

func TestAssembleAlert(t *testing.T) {
	assert := assert.New(t)
	s, e := base.SettingsBootStrap("test_settings.db")
	assert.NoError(e)

	httpServer := httptest.NewServer(base.Routes())
	base.SettingsBootStrap("test_settings.db")

	set := map[string]interface{}{"SMTPHost": "smtp.example.com", "SMTPUser": "human@example.com", "SMTPPass": "sample", "SMTPPort": 587}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	d := Device{Nickname: "test"}

	a, e := AssembleAlertEmail("flood", d)
	assert.NoError(e)
	assert.NotNil(a.Auth)
	assert.Equal([]string([]string{s.Email}), a.To)
	assert.Equal("flood", a.Type)
	msg := "To: " + s.Email + "\r\n" +
		"Subject: Packetpeek - " + d.Nickname + " Flood Alert!\r\n\r\nYour BIOT reported a flood alert!\r\n"
	assert.Equal([]byte(msg), a.Message)

	a, _ = AssembleAlertEmail("door", d)
	assert.Equal("door", a.Type)
	a, _ = AssembleAlertEmail("water", d)
	assert.Equal("water", a.Type)

	tools.DeleteTestDatabases("test_settings.db")
}
