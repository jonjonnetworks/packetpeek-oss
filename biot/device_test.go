package biot

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

// CREATE

func TestAttemptAutoRegistration(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	d := makeDevicePacket()
	var dev Device
	assert.NoError(json.Unmarshal(d, &dev))
	assert.NoError(AttemptAutoRegistration(dev))
	// Fail on duplicate, dont register existing devices even though we should never reach that point
	assert.Error(AttemptAutoRegistration(dev))

	QuickTeardown(t)
}

func TestAttemptAutoRegistrationSetsCorrectDefaults(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	dev := Device{
		DeviceID: "1a:2b:3c:4d:5e:6f",
		Nickname: "test",
	}
	assert.NoError(AttemptAutoRegistration(dev))

	httpServer := httptest.NewServer(Routes())
	req, err := http.Get(httpServer.URL + "/biot/device/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("default", resp["melody"])
	assert.Equal("p", resp["dooralert"])
	// Decoding throws all numbers into floats to be safe+lazy
	assert.Equal(float64(6), resp["updateHours"])
	assert.Equal(float64(0), resp["updateMinutes"])
	assert.Equal(float64(15), resp["alertMinutes"])
	assert.Equal(float64(1), resp["mode"])

	QuickTeardown(t)
}

func TestDeviceCreate(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	QuickTeardown(t)
}

func TestDeviceCreateFailsOnInsertError(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()
	registerTestDevice(httpServer, t)

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusConflict, req.StatusCode)

	QuickTeardown(t)
}

func TestDeviceCreateDefaultsUpdateAndAlertTiming(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := []byte(`{"id":"1a:2b:3c:4d:5e:6f","nickname":"testdevice"}`)

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/biot/device/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	QuickTeardown(t)
}

// READ

func TestDeviceRead(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/biot/device/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("1a:2b:3c:4d:5e:6f", resp["id"])

	QuickTeardown(t)
}

func TestDeviceReadBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/biot/device/bad_id")
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, req.StatusCode)

	QuickTeardown(t)
}

// READ ALL

func TestDeviceReadAll(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/biot/devices")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	var resp []map[string]interface{}
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("1a:2b:3c:4d:5e:6f", resp[0]["id"])

	QuickTeardown(t)
}

// UPDATE

func TestDeviceUpdate(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	device2 := map[string]interface{}{
		"DeviceID":      "99:99:99:99:99:99",
		"Nickname":      "newname",
		"UpdateHours":   99,
		"UpdateMinutes": 99,
		"AlertMinutes":  99,
	}

	data2Json, err := json.Marshal(device2)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/biot/device/1a:2b:3c:4d:5e:6f", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	QuickTeardown(t)
}

func TestDeviceUpdateBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	device22 := Device{
		DeviceID:      "99:99:99:99:99:99",
		Nickname:      "newname",
		UpdateHours:   99,
		UpdateMinutes: 99,
		AlertMinutes:  99,
	}

	data2Json, err := json.Marshal(device22)
	assert.NoError(err)

	req3, err := http.NewRequest("PATCH", httpServer.URL+"/biot/device/9999", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req3)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	QuickTeardown(t)
}

func TestDeviceUpdateBadPayloadReturnsNil(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	badBytes := []byte(`badbytes`)
	req4, err := http.NewRequest("PATCH", httpServer.URL+"/biot/device/1a:2b:3c:4d:5e:6f", bytes.NewBuffer(badBytes))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req4)
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, resp.StatusCode)

	QuickTeardown(t)
}

// DELETE

func TestDeviceDelete(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/biot/device/1a:2b:3c:4d:5e:6f", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNoContent, resp.StatusCode)

	QuickTeardown(t)
}

func TestDeviceDeleteBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/biot/device/9999", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	QuickTeardown(t)
}

// HELPER FUNCTIONS

func makeDevicePacket() []byte {
	return []byte(`{"id":"1a:2b:3c:4d:5e:6f","nickname":"testdevice","updateHours":6,"updateMinutes":0,"alertMinutes":15,"melody":"default"}`)
}
