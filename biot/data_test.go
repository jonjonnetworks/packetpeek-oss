package biot

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// CREATE

func TestDataCreateFromClientUnRegisteredBIOT(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(makeDataPacket().Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	// Ensure device is now autoregistered
	req, err = http.Get(httpServer.URL + "/biot/device/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	QuickTeardown(t)
}

func TestAutoRegisterFailsOnBadData(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())

	badData := url.Values{}
	badData.Set("junk", "nogood")

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(badData.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, req.StatusCode)

	QuickTeardown(t)
}

func TestDataCreateFromClient(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(makeDataPacket().Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	QuickTeardown(t)
}

// READ

func TestDataRead(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/biot/data/1a:2b:3c:4d:5e:6f")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("1a:2b:3c:4d:5e:6f", resp["deviceID"])

	QuickTeardown(t)
}

func TestDataReadBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/biot/data/99:99:99:99:99:99")
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, req.StatusCode)

	QuickTeardown(t)
}

// READ ALL

func TestDataReadAll(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/biot/data")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	var resp []map[string]interface{}
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal("1a:2b:3c:4d:5e:6f", resp[0]["deviceID"])

	QuickTeardown(t)
}

// UPDATE

func TestDataUpdate(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	data2 := map[string]interface{}{
		"DeviceID":    "99:99:99:99:99:99",
		"Battery":     1000,
		"Temperature": 1000,
		"Moisture":    100,
		"HallSense":   "d",
		"Nickname":    "UPDATED",
	}

	data2Json, err := json.Marshal(data2)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/biot/data/1", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	QuickTeardown(t)
}

func TestDataUpdateBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	data2 := map[string]interface{}{
		"DeviceID":    "99:99:99:99:99:99",
		"Battery":     1000,
		"Temperature": 1000,
		"Moisture":    100,
		"HallSense":   "d",
		"Nickname":    "UPDATED",
	}

	data2Json, err := json.Marshal(data2)
	assert.NoError(err)

	req3, err := http.NewRequest("PATCH", httpServer.URL+"/biot/data/99:99:99:99:99:99", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req3)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	QuickTeardown(t)
}

func TestDataUpdateBadPayloadReturnsNil(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	badBytes := []byte(`badbytes`)
	req4, err := http.NewRequest("PATCH", httpServer.URL+"/biot/data/1", bytes.NewBuffer(badBytes))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req4)
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, resp.StatusCode)

	QuickTeardown(t)
}

// DELETE

func TestDataDelete(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/biot/data/1", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNoContent, resp.StatusCode)

	QuickTeardown(t)
}

func TestDataDeleteBadIDReturnsNil(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())
	registerTestDevice(httpServer, t)

	data := makeDataPacket()

	req, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/biot/data/99:99:99:99:99:99", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	QuickTeardown(t)
}

func TestGenerateBiotResponse(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())

	registerTestDevice(httpServer, t)

	data := Data{DeviceID: "1a:2b:3c:4d:5e:6f"}
	song := generateBiotResponse(data)
	assert.Equal(uint8(0xCC), song[2])
	assert.Equal(uint8(0x06), song[3])
	assert.Equal(uint8(0x00), song[4])
	assert.Equal(uint8(0x0f), song[5])
	assert.Equal(uint8(0x9A), song[64])
	assert.Equal(uint8(0x5E), song[65])
	assert.Equal(uint8(0x3A), song[66])

	// Ensure we get error tones on orphaned data
	data2 := Data{DeviceID: "ff:ff:ff:ff:ff:ff"}
	song2 := generateBiotResponse(data2)
	assert.Equal(uint8(0xCC), song2[2])
	assert.Equal(uint8(0x3A), song2[64])
	assert.Equal(uint8(0x5E), song2[65])
	assert.Equal(uint8(0x9A), song2[66])

	QuickTeardown(t)
}

func TestUpdateBiotSettingsThroughResponse(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	httpServer := httptest.NewServer(Routes())

	registerTestDevice(httpServer, t)

	device := map[string]interface{}{
		"DeviceID":      "1a:2b:3c:4d:5e:6f",
		"UpdateHours":   99,
		"UpdateMinutes": 99,
		"AlertMinutes":  99,
	}

	data2Json, err := json.Marshal(device)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/biot/device/1a:2b:3c:4d:5e:6f", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	data := Data{DeviceID: "1a:2b:3c:4d:5e:6f"}
	song := generateBiotResponse(data)

	assert.Equal(uint8(0xCC), song[2])
	assert.Equal(uint8(0x63), song[3])
	assert.Equal(uint8(0x63), song[4])
	assert.Equal(uint8(0x63), song[5])
	assert.Equal(uint8(0x9A), song[64])
	assert.Equal(uint8(0x5E), song[65])
	assert.Equal(uint8(0x3A), song[66])

	QuickTeardown(t)
}

func TestGetBiotSong(t *testing.T) {
	assert := assert.New(t)
	assert.Equal("\xFF", getBiotSong("silent"))
	assert.Equal("\x9A\x5E\x3A", getBiotSong("default"))
	assert.Equal("\x3A\x5E\x9A", getBiotSong("error"))
	assert.Equal("\x3A\x5E\x3A\x5E\x3A\x5E\x3A\x5E", getBiotSong("alert"))
	assert.Equal("\x3A\x3A\x3A", getBiotSong("undefined"))
}

// HELPER FUNCTIONS

func registerTestDevice(httpServer *httptest.Server, t *testing.T) {
	assert := assert.New(t)

	device := makeDevicePacket()

	req, err := http.Post(httpServer.URL+"/biot/device", "application/x-www-form-urlencoded", bytes.NewBuffer(device))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)
}

func makeDataPacket() url.Values {
	data := url.Values{}
	data.Set("id", "1a:2b:3c:4d:5e:6f")
	data.Set("bat_lev", "2905")
	data.Set("temp", "2995")
	data.Set("hum", "240")
	data.Set("door", "q")
	data.Set("nick", "testdevice")

	return data
}
