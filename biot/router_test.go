package biot

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jonjonnetworks/packetpeek-oss/tools"
)

const testDevicesTable = "test_biot_devices.db"
const testDataTable = "test_biot_data.db"
const testSettingsTable = "test_biot_settings.db"

// ROUTER

func TestRouter(t *testing.T) {
	assert := assert.New(t)

	QuickInit()

	assert.NotNil(Routes())

	QuickTeardown(t)
}

func QuickInit() {
	BIOTInit(testDevicesTable, testDataTable, testSettingsTable)
}

func QuickTeardown(t *testing.T) {
	assert := assert.New(t)
	assert.NoError(tools.DeleteTestDatabases(testDevicesTable, testDataTable, testSettingsTable))
}
