package biot

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStartup(t *testing.T) {
	assert := assert.New(t)

	p := startup(testDevicesTable, testDataTable, testSettingsTable)
	assert.Equal(7875, p)
	QuickTeardown(t)
}
