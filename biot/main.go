package biot

import (
	"fmt"
	"net/http"

	"github.com/alexbyk/panicif"
	log "github.com/sirupsen/logrus"
)

func startup(dev, dat, set string) int {
	BIOTInit(dev, dat, set)
	panicif.Err(SettingsImport())

	var s Settings
	SettingsDB.First(&s)

	http.Handle("/dev/f1", BIOTEndpoint(s.BIOTPort))
	log.Infof("Starting BIOT device server on port %d...\n", s.BIOTPort)
	return s.BIOTPort
}

func Start() error {
	return http.ListenAndServe(fmt.Sprintf(":%d", startup("biot_devices.db", "biot_data.db", "biot_settings.db")), nil)
}
