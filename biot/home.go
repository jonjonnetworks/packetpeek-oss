package biot

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/jonjonnetworks/packetpeek-oss/base"
)

type Server struct {
	Version string
	Release string
	ID      string
}

// Home contains everything needed to render the dashboard
type HomeStruct struct {
	Server             Server
	BaseSettings       base.Settings
	BiotServerSettings Settings
	BiotDeviceSettings Device
	DeviceList         []string
	Data               []Data
}

// Home serves the data to create the dashboard
func Home(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]
	var home HomeStruct

	var devices []Device
	DeviceDB.Find(&devices)

	// home.DeviceList = getIDs(devices)

	SettingsDB.First(&home.BiotServerSettings)

	listStyle := home.BiotServerSettings.DeviceListStyle

	switch listStyle {
	case "device_id":
		home.DeviceList = getIDs(devices)
	case "nickname":
		home.DeviceList = getNicknames(devices)
	default:
		home.DeviceList = getIDs(devices)
	}

	if id == "" {
		if len(home.DeviceList) == 0 {
			id = "none"
		} else {
			id = home.DeviceList[0]
		}
	}

	switch listStyle {
	case "device_id":
		home.BiotDeviceSettings = getDeviceByID(id, devices)
	case "nickname":
		devByNick := getDeviceByNickname(id, devices)
		home.BiotDeviceSettings = devByNick
		id = devByNick.DeviceID
	}

	base.SettingsDB.First(&home.BaseSettings)
	DeviceDB.Where("device_id = ?", id).First(&home.BiotDeviceSettings)

	version := "2.2.1"
	server := Server{
		Version: "v" + version + "-OSS",
		Release: "Dec-2022",
		ID:      id,
	}
	home.Server = server

	gp := home.BiotServerSettings.GraphPoints
	if gp == 0 {
		gp = 75
	}

	DataDB.Limit(gp).Where("device_id = ?", id).Order("created_at desc").Find(&home.Data)

	// fmt.Println(home.BiotServerSettings)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(home)
}

func getIDs(devices []Device) []string {
	var ids []string
	for _, v := range devices {
		ids = append(ids, v.DeviceID)
	}
	return ids
}

func getNicknames(devices []Device) []string {
	var names []string
	for _, v := range devices {
		names = append(names, v.Nickname)
	}
	return names
}

func getDeviceByID(id string, devices []Device) Device {
	for _, d := range devices {
		if d.DeviceID == id {
			return d
		}
	}
	return Device{}
}

func getDeviceByNickname(nick string, devices []Device) Device {
	for _, d := range devices {
		if d.Nickname == nick {
			return d
		}
	}
	return Device{}
}
