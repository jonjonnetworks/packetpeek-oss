package biot

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jonjonnetworks/packetpeek-oss/base"
)

func TestHome(t *testing.T) {
	assert := assert.New(t)

	QuickInit()
	base.SettingsInit(testSettingsTable)

	httpServer := httptest.NewServer(Routes())

	// Test before devices exist to get ID of "none"
	noneReq, err := http.Get(httpServer.URL + "/biot/home")
	assert.NoError(err)
	assert.Equal(http.StatusOK, noneReq.StatusCode)
	var none HomeStruct
	noneDecoder := json.NewDecoder(noneReq.Body)
	err = noneDecoder.Decode(&none)
	assert.NoError(err)
	assert.Equal("none", none.Server.ID)

	registerTestDevice(httpServer, t)
	data := makeDataPacket()
	// device := makeDevicePacket()

	dataReq, err := http.Post(httpServer.URL+"/biot/datum", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, dataReq.StatusCode)

	homeReq, err := http.Get(httpServer.URL + "/biot/home")
	assert.NoError(err)
	assert.Equal(http.StatusOK, homeReq.StatusCode)

	var resp HomeStruct
	decoder := json.NewDecoder(homeReq.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)

	assert.Equal("1a:2b:3c:4d:5e:6f", resp.BiotDeviceSettings.DeviceID)
	assert.Equal("testdevice", resp.Data[0].Nickname)

	QuickTeardown(t)
}

func TestGetIDs(t *testing.T) {
	assert := assert.New(t)
	assert.Equal([]string{"aa:aa:aa:aa:aa:aa", "bb:bb:bb:bb:bb:bb"}, getIDs(devList()))
}

func TestGetNicknames(t *testing.T) {
	assert := assert.New(t)
	assert.Equal([]string{"test1", "test2"}, getNicknames(devList()))
}

func TestGetDeviceByID(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(Device{
		DeviceID:      "bb:bb:bb:bb:bb:bb",
		Nickname:      "test2",
		UpdateHours:   6,
		UpdateMinutes: 0,
	}, getDeviceByID("bb:bb:bb:bb:bb:bb", devList()))
}

func devList() []Device {
	return []Device{
		{
			DeviceID:      "aa:aa:aa:aa:aa:aa",
			Nickname:      "test1",
			UpdateHours:   6,
			UpdateMinutes: 0,
		},
		{
			DeviceID:      "bb:bb:bb:bb:bb:bb",
			Nickname:      "test2",
			UpdateHours:   6,
			UpdateMinutes: 0,
		},
	}
}
