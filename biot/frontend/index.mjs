// mini lib to clean up code a little
// finds the element matching the selector, or returns the element if an element is passed
const $ = (selector) =>
  typeof selector === 'string' ? document.querySelector(selector) : selector
const $$ = (selector) => document.querySelectorAll(selector)
// register an event handler on a target
// - if no target is given it assumes document
// - if an array is given it will register it for all elements in the array
const on = (target, event, handler) =>
  handler
    ? target.forEach
      ? target.forEach((el) => el.addEventListener(event, handler))
      : $(target).addEventListener(event, handler)
    : document.addEventListener(target, event)

on(window, 'DOMContentLoaded', () => fetchData(''))

// Menu toggle
let $burger = $('#menu-burger')
let $menu = $('#menu')

on($burger, 'click', (event) => {
  event.stopPropagation()
  closeDropdowns()
  $menu.classList.toggle('is-active')
})

// Closing controls

const $$dropdowns = $$('.dropdown:not(.is-hoverable)')
function closeDropdowns() {
  $$dropdowns.forEach((el) => el.classList.remove('is-active'))
}

on('keydown', (event) => {
  let e = event || window.event
  if (e.key.startsWith('Esc')) {
    closeDropdowns()
    closeModals()
  }
})
on('click', (event) => {
  closeDropdowns()
})

// make modal backgrounds clickable
on($$('.modal-background'), 'click', closeModals)

const $$modals = $$('.modal')
function closeModals() {
  $$modals.forEach((el) => el.classList.remove('is-active'))
}

// Hide discord notification
// on('#discord-close', 'click', (event) => {
//   event.stopPropagation()
//   $('#discord-link').classList.toggle('is-hidden')
// })

// Server Modal toggle
let $serverSettingsModal = $('#server-settings-modal')
on('#server-settings', 'click', (event) => {
  event.stopPropagation()
  closeDropdowns()
  $serverSettingsModal.classList.toggle('is-active')
})
on('#server-settings-close', 'click', (event) => {
  event.stopPropagation()
  $serverSettingsModal.classList.toggle('is-active')
})

// Server SMTP Settings toggle
on('#server-smtp-settings', 'click', (event) => {
  event.stopPropagation()
  $('#smtpsettings').classList.toggle('is-hidden')
})

// Server SMTP Settings toggle
on('#server-port-settings', 'click', (event) => {
  event.stopPropagation()
  $('#portsettings').classList.toggle('is-hidden')
})

// Device Modal toggle
on('#device-settings', 'click', (event) => {
  event.stopPropagation()
  closeDropdowns()
  $('#device-settings-modal').classList.toggle('is-active')
})

on('#device-settings-close', 'click', (event) => {
  event.stopPropagation()
  $('#device-settings-modal').classList.remove('is-active')
})

// Explanation function
on('#portrangehelp', 'click', (event) => {
  event.stopPropagation()
  alert(
    'In order to avoid needing root permissions, the server will avoid ports under 1024, and limit the port to a real one below 65,535. Disabling this allows you to input any number. Use with caution.'
  )
})

// Ensure port settings are in proper, non-sudo range
function setPortInRange(e, id) {
  console.log(e, id)
  if (e < 1024) {
    document.querySelector('#' + id).value = 1024
  } else if (e > 65535) {
    document.querySelector('#' + id).value = 65535
  }
}

// Server Settings submit
on('#server-settings-submit', 'click', () => {
  fetch('/settings', {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      Name: $('#name').value,
      Email: $('#email').value,
      Timezone: $('#timezone').value,
      Units: $('#units').value,
      Port: parseInt($('#port').value),
      LimitPorts: $('#limitports').checked,
      SMTPUser: $('#smtpuser').value,
      SMTPPass: $('#smtppass').value,
      SMTPHost: $('#smtphost').value,
      SMTPPort: parseInt($('#smtpport').value),
      Alerts: $('#alerts').checked
    })
  })
    .then((res) => {
      if (res.status == 200) {
        fetchData($('#id').innerText)
      } else {
        alert('An error occured, ReadOnly?')
      }
    })
    .then(() => {
      fetch('/biot/settings', {
        method: 'PATCH',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          BIOTPort: parseInt($('#biotport').value),
          GraphPoints: parseInt($('#graphdp').value),
          DeviceListStyle: $('#devliststyle').value
        })
      }).then((res) => {
        if (res.status == 200) {
          fetchData($('#id').innerText)
          $serverSettingsModal.classList.toggle('is-active')
        } else {
          alert('An error occured, ReadOnly?')
        }
      })
    })
    .catch((err) => alert(err))
})

// Device Settings submit
on('#device-settings-submit', 'click', (event) => {
  fetch('/biot/device/' + $('#id').innerText, {
    method: 'PATCH',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      Nickname: $('#nicknamesetting').value,
      Melody: $('#melodysetting').value,
      UpdateHours: parseInt($('#intervalhoursetting').value),
      UpdateMinutes: parseInt($('#intervalminutesetting').value),
      DoorAlert: $('#dooralertsetting').value,
      AlertMinutes: parseInt($('#alertminutesetting').value) //,
      // Mode: parseInt($("#devicemode").value)
    })
  })
    .then((res) => {
      if (res.status == 200) {
        fetchData($('#id').innerText)
        $('#device-settings-modal').classList.toggle('is-active')
      } else {
        alert('An error occured, ReadOnly?')
      }
    })
    .catch((err) => alert(err))
})

let tempUnit = ''
async function fetchData(id) {
  try {
    const data = await fetch(`/biot/home/${id}`).then((res) => res.json())
    const settings = data.BaseSettings
    const biotServerSettings = data.BiotServerSettings

    console.log(data)

    $('#name').value = settings.Name
    $('#email').value = settings.Email
    $('#timezone').value = settings.Timezone
    $('#units').value = settings.Units
    $('#port').value = settings.Port
    $('#biotport').value = biotServerSettings.BIOTPort
    $('#limitports').checked = settings.LimitPorts
    $('#smtpuser').value = settings.SMTPUser
    $('#smtppass').value = settings.SMTPPass
    $('#smtphost').value = settings.SMTPHost
    $('#smtpport').value = settings.SMTPPort
    $('#alerts').checked = settings.Alerts
    $('#graphdp').value = biotServerSettings.GraphPoints
    $('#devliststyle').value = biotServerSettings.DeviceListStyle

    const deviceData = data.Data[0]
    const deviceInfo = data.BiotDeviceSettings

    $('#version').innerHTML = data['Server']['Version']
    $('#release').innerHTML = data['Server']['Release']
    $('#nickname').innerHTML = deviceData.nickname
    $('#id').innerHTML = deviceData.deviceID

    tempUnit = settings.Units == 'Metric' ? 'C' : 'F'
    const temperature =
      settings.Units == 'Metric'
        ? deviceData.temperaturecelsius
        : deviceData.temperaturefahrenheit
    $('#temperature').innerHTML = temperature + '&deg;' + tempUnit

    $('#moisture').innerHTML = deviceData.moisturepercent + '%'
    $('#battery').innerHTML = deviceData.batterypercent + '%'
    $('#door').innerHTML = deviceData.doorstatus

    $('#intervalhoursetting').value = deviceInfo.updateHours
    $('#intervalminutesetting').value = deviceInfo.updateMinutes
    $('#melodysetting').value = deviceInfo.melody
    $('#nicknamesetting').value = deviceInfo.nickname
    $('#dooralertsetting').value = deviceInfo.dooralert
    $('#alertminutesetting').value = deviceInfo.alertMinutes

    updateDeviceList(data.DeviceList)

    buildGraphs(data, biotServerSettings, settings)
  } catch {
    displayProblemScreen()
  }
}

function updateDeviceList(devices) {
  const deviceList = $('#deviceList')

  // clear list
  while (deviceList.firstChild) deviceList.firstChild.remove()

  deviceList.append(
    ...devices.map((device) => {
      console.log(device)
      const item = document.createElement('span')
      item.className = 'navbar-item fake-link'
      item.onclick = () => fetchData(device)
      item.innerText = device
      return item
    })
  )
}

function displayProblemScreen() {
  $('#device-settings').classList.add('is-hidden')
  $('#no-data').classList.remove('is-hidden')
  $('#nickname').innerHTML = 'Unavailable'
  $('#id').innerHTML = 'Is it running?'
  $('#temperature').innerHTML = 'N/A'
  $('#moisture').innerHTML = 'N/A'
  $('#battery').innerHTML = 'N/A'
  $('#door').innerHTML = 'N/A'
}

function buildGraphs(data, biotsettings, settings) {
  let gd = parseGraphData(data, settings)
  let gp = biotsettings.GraphPoints

  // Create graph canvases
  cleanCanvases()

  let graphOptions = {
    title: {
      display: true,
      fontColor: 'lightgrey'
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { display: false },
    scales: {
      xAxes: [
        {
          display: true,
          gridLines: {
            display: false
          },
          ticks: {
            display: false
          },
          stacked: true
        }
      ],
      yAxes: [
        {
          display: true,
          gridLines: {
            display: true,
            color: 'rgba(128, 128, 128, 0.2)',
            drawTicks: true,
            drawBorder: false,
            z: -1
          },
          ticks: {
            beginAtZero: true,
            display: true
          }
        }
      ]
    }
  }

  const temperatureCanvas = $('#graph-data-temperature')
  const temperatureContext = temperatureCanvas.getContext('2d')
  const temperatureChart = new Chart(temperatureContext, {
    type: 'bar',
    data: {
      labels: gd.created,
      datasets: [
        {
          label: 'Temperature',
          data: gd.tmp,
          backgroundColor: 'rgba(204, 15, 53, 0.8)',
          borderColor: 'rgba(204, 15, 53, 1)',
          borderWidth: 1
        }
      ]
    },
    options: graphOptions
  })
  temperatureChart.options.title.text = `Temperature (Last ${gp} Entries)`
  temperatureChart.options.tooltips.callbacks.label = (item) =>
    `${item.yLabel}°${tempUnit}`

  let moistureContext = $('#graph-data-moisture').getContext('2d')
  let moistureChart = new Chart(moistureContext, {
    type: 'bar',
    data: {
      labels: gd.created,
      datasets: [
        {
          label: 'Moisture',
          data: gd.mst,
          backgroundColor: 'rgba(32, 156, 238, 0.8)',
          borderColor: 'rgba(32, 156, 238, 1)',
          borderWidth: 1
        }
      ]
    },
    options: graphOptions
  })
  moistureChart.options.title.text = `Moisture (Last ${gp} Entries)`
  moistureChart.options.tooltips.callbacks.label = (item) =>
    `${item.yLabel}% RH`

  const batteryContext = $('#graph-data-battery').getContext('2d')
  const batteryChart = new Chart(batteryContext, {
    scaleOverride: true,
    scaleStartValue: 0,
    type: 'bar',
    data: {
      labels: gd.created,
      datasets: [
        {
          label: 'Battery',
          data: gd.bat,
          backgroundColor: 'rgba(37, 121, 66, 0.8)',
          borderColor: 'rgba(37, 121, 66, 1)',
          borderWidth: 1
        }
      ]
    },
    options: graphOptions
  })
  batteryChart.options.title.text = `Battery (Last ${gp} Entries)`
  batteryChart.options.tooltips.callbacks.label = (item) => `${item.yLabel}%`

  const doorstatusContext = document
    .querySelector('#graph-data-doorstatus')
    .getContext('2d')
  const doorstatusChart = new Chart(doorstatusContext, {
    scaleOverride: true,
    scaleStartValue: 0,
    type: 'bar',
    data: {
      labels: gd.dsl,
      datasets: [
        {
          label: 'Times',
          data: gd.ds,
          backgroundColor: 'rgba(148, 118, 0, 0.8)',
          borderColor: 'rgba(148, 118, 0, 1)',
          borderWidth: 1
        }
      ]
    },
    options: graphOptions
  })
  doorstatusChart.options.title.text = `Door Events (Last ${gp} Entries)`
  // doorstatusChart.options.scales.xAxis.gridLines.display = true
  doorstatusChart.options.tooltips.callbacks.label = (item) => `${item.yLabel}x`
}

function cleanCanvases() {
  $('#graph-data-temperature').remove()
  $('#graph-container-temperature').innerHTML =
    '<canvas class="dgraph" id="graph-data-temperature"></canvas>'

  $('#graph-data-moisture').remove()
  $('#graph-container-moisture').innerHTML =
    '<canvas class="dgraph" id="graph-data-moisture"></canvas>'

  $('#graph-data-battery').remove()
  $('#graph-container-battery').innerHTML =
    '<canvas class="dgraph" id="graph-data-battery"></canvas>'

  $('#graph-data-doorstatus').remove()
  $('#graph-container-doorstatus').innerHTML =
    '<canvas class="dgraph" id="graph-data-doorstatus"></canvas>'
}

function parseGraphData(data, settings) {
  let created = []
  let tmp = []
  let mst = []
  let bat = []
  let ds = []
  let dsl = []

  let open = 0
  let wopen = 0
  let close = 0
  let closed = 0

  for (const d of data['Data'].reverse()) {
    let dt = new Date(Date.parse(d['CreatedAt']))
    created.push(dt.toLocaleDateString() + ' ' + dt.toLocaleTimeString())

    switch (settings.Units) {
      case 'Metric':
        tmp.push(d['temperaturecelsius'])
        break
      case 'Imperial':
        tmp.push(d['temperaturefahrenheit'])
        break
    }

    mst.push(d['moisturepercent'])
    bat.push(d['batterypercent'])
    switch (d['doorstatus']) {
      case 'Recently Opened':
        open += 1
        break
      case 'Wide Open':
        wopen += 1
        break
      case 'Recently Closed':
        close += 1
        break
      case 'Closed Shut':
        closed += 1
        break
    }
  }
  ds.push(open, wopen, close, closed)
  dsl.push('Recently Opened', 'Wide Open', 'Recently Closed', 'Closed Shut')

  return { created, tmp, mst, bat, ds, dsl }
}
