package biot

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/jonjonnetworks/packetpeek-oss/base"
	"gitlab.com/jonjonnetworks/packetpeek-oss/tools"
)

type Alert struct {
	Email  string
	Reason string
}

func manageAlerts(dev Device, d Data) error {
	//Check for alerts
	isAlert, reason := checkAlert(dev, d)
	if isAlert {
		a, err := AssembleAlertEmail(reason, dev)
		if err != nil {
			log.Warn(err)
			return err
		}

		err = tools.SendAlert(a)
		if err != nil {
			log.Warn(err)
			return err
		}
	}
	return nil
}

func checkAlert(dev Device, data Data) (bool, string) {
	isAlert := false
	var reason string

	hs := data.HallSense
	if hs == "" {
		return false, ""
	}

	// r = door open, f = door closed, both mean flood alert
	if hs == "r" || hs == "f" {
		isAlert = true
		reason = "flood"
		if dev.Mode == 2 {
			reason = "water"
		}
	} else if hs == dev.DoorAlert {
		isAlert = true
		reason = "door"
	}
	return isAlert, reason
}

func AssembleAlertEmail(reason string, d Device) (tools.Alert, error) {
	// Fetch user info
	s := base.GetSettings()

	if s.SMTPUser == "example@example.com" || s.SMTPPass == "examplepassword" {
		return tools.Alert{}, fmt.Errorf("No SMTP credentials, cant send " + reason + " alert!")
	}

	msgContent := "To: " + s.Email + "\r\n" + "Subject: Packetpeek - " + d.Nickname + " "
	switch reason {
	case "flood":
		msgContent += floodtemplate
	case "door":
		msgContent += doortemplate
	case "water":
		msgContent += watertemplate
	}

	al := tools.Alert{
		Type:    reason,
		To:      []string{s.Email},
		Message: []byte(msgContent),
		Auth:    tools.LoginAuth(s.SMTPUser, s.SMTPPass),
		SMTP: tools.SMTP{
			User: s.SMTPUser,
			Pass: s.SMTPPass,
			Host: s.SMTPHost,
			Port: s.SMTPPort,
		},
	}

	return al, nil
}

var floodtemplate = "Flood Alert!\r\n\r\nYour BIOT reported a flood alert!\r\n"
var watertemplate = "Just Got Watered!\r\n\r\nYour BIOT reported being watered!\r\n"
var doortemplate = "Door Alert!\r\n\r\nYour BIOT reported a door alert!\r\n"
