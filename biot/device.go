package biot

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// Device holds our BIOT registration fields
type Device struct {
	gorm.Model
	DeviceID      string `json:"id" gorm:"primaryKey;unique"`
	Nickname      string `json:"nickname"`
	Melody        string `json:"melody"`
	DoorAlert     string `json:"dooralert"`
	UpdateHours   int    `json:"updateHours"`
	UpdateMinutes int    `json:"updateMinutes"`
	AlertMinutes  int    `json:"alertMinutes"`
	Mode          int    `json:"mode"`
}

type DeviceID struct {
	DeviceID string `json:"id"`
}

// AttemptAutoRegistration will attempt to register unknown devices posting data, if the ID is a MAC
func AttemptAutoRegistration(d Device) error {
	_, err := net.ParseMAC(d.DeviceID)
	if err != nil {
		return err
	}

	// Device Modes:
	// 0: disabled
	// 1: general purpose, all features enabled
	// 2: Plant sensor: PH sensing, temperature, moisture. Set flood alrts to say 'watering detected' instead, hide door sensor
	// 3: Access Monitoring: temperature, hide moisture, hide ph
	// 4: Flood Detection: temperature, moisture, hide ph, hide door sensor

	// Define defaults if the device didn't send it
	if d.UpdateHours == 0 && d.UpdateMinutes == 0 && d.AlertMinutes == 0 && d.Mode == 0 {
		d.UpdateHours = 6
		d.UpdateMinutes = 0
		d.AlertMinutes = 15
		d.Mode = 1
	}

	device := Device{
		DeviceID:      d.DeviceID,
		Nickname:      d.Nickname,
		Melody:        "default",
		DoorAlert:     "p",
		UpdateHours:   d.UpdateHours,
		UpdateMinutes: d.UpdateMinutes,
		AlertMinutes:  d.AlertMinutes,
		Mode:          d.Mode,
	}

	return DeviceDB.Create(&device).Error
}

// DeviceCreate handles when a Device registers with the server
func DeviceCreate(w http.ResponseWriter, r *http.Request) {
	device := Device{
		Melody:        "default",
		DoorAlert:     "p", // When door opens
		UpdateHours:   6,
		UpdateMinutes: 0,
		AlertMinutes:  15,
		Mode:          1,
	}
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&device)

	if result := DeviceDB.Create(&device); result.Error != nil {
		w.WriteHeader(http.StatusConflict)
		w.Write([]byte(fmt.Sprintf(`{ "message": "Insert Error: %v" }`, result.Error)))
	} else {
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(`{ "message": "Succesfully created device" }`))
	}
}

// DeviceRead will retrieve the device with specified ID
func DeviceRead(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var device Device
	if DeviceDB.Where("device_id = ?", id).First(&device).RowsAffected < 1 {
		respJSON := []byte(`{ "message": "Device not found" }`)
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(device)
}

// DeviceReadAll will return all devices
func DeviceReadAll(w http.ResponseWriter, r *http.Request) {
	var device []Device

	DeviceDB.Find(&device)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(device)
}

// DeviceUpdate will update the values for the device with specified ID
func DeviceUpdate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var deviceUpdate map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&deviceUpdate); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{ "message": "Error Unmarshaling JSON" }`))
		return
	}

	var device Device
	if DeviceDB.Where("device_id = ?", id).First(&device).Update(deviceUpdate).RowsAffected < 1 {
		respJSON := []byte(`{ "message": "Device not found" }`)
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(device)
}

// DeviceDelete will delete the device with specified ID, including all of its data
func DeviceDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var device Device
	if DeviceDB.Where("device_id = ?", id).First(&device).Delete(&device).RowsAffected > 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte(`{ "message": "No device was found by that ID" }`))
}

// This function is here so we can reuse h from Router
func validateDevice(id string) bool {
	var device Device
	return DeviceDB.Where("device_id = ?", id).First(&device).RowsAffected > 0
}
