package biot

import (
	"mime"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/jonjonnetworks/packetpeek-oss/base"
)

// Router creates our Mux routing
func Routes() *mux.Router {
	mux := mux.NewRouter().PathPrefix("/biot/").Subrouter().StrictSlash(true)

	mux.HandleFunc("/data", DataReadAll).Methods("GET")
	mux.HandleFunc("/data/{id}", DataRead).Methods("GET")

	mux.HandleFunc("/devices", DeviceReadAll).Methods("GET")
	mux.HandleFunc("/device/{id}", DeviceRead).Methods("GET")

	mux.HandleFunc("/home", Home).Methods("GET")
	mux.HandleFunc("/home/{id}", Home).Methods("GET")

	mux.HandleFunc("/settings", SettingsRead).Methods("GET")

	mime.AddExtensionType(".mjs", "text/javascript")
	// fs := http.StripPrefix("/biot/", http.FileServer(http.Dir("./biot/frontend")))
	fs := http.StripPrefix("/biot/", http.FileServer(http.Dir(filepath.Join("biot", "frontend"))))

	mux.PathPrefix("/assets/").Handler(fs)
	mux.PathPrefix("/css/").Handler(fs)
	mux.Handle("/index.mjs", fs)
	mux.Handle("/", fs)

	// Dont include Create, Update, or Delete routes
	if os.Getenv("PPKENV") == "READONLY" {
		log.Warn("*** Server in ReadOnly mode! ***")
		return mux
	}

	mux.HandleFunc("/datum", func(w http.ResponseWriter, r *http.Request) {
		// POST request does not come from a BIOT
		statusCode, responseBody := DataCreate(w, r, false)
		w.WriteHeader(statusCode)
		w.Write(responseBody)
	}).Methods("POST")
	mux.HandleFunc("/data/{id}", DataUpdate).Methods("PATCH")
	mux.HandleFunc("/data/{id}", DataDelete).Methods("DELETE")

	mux.HandleFunc("/device", DeviceCreate).Methods("POST")
	mux.HandleFunc("/device/{id}", DeviceUpdate).Methods("PATCH")
	mux.HandleFunc("/device/{id}", DeviceDelete).Methods("DELETE")

	mux.HandleFunc("/settings", SettingUpdate).Methods("PATCH")

	mux.HandleFunc("/archive", func(w http.ResponseWriter, r *http.Request) {
		if base.ArchiveEnabled {
			a := base.Archiver{
				SRCHandler:    DataDB,
				DSTHandler:    ArchiveDB,
				LeaveXRecords: 10,
			}
			rows, err := base.Archive(a)
			if err != nil {
				w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
				w.WriteHeader(http.StatusInternalServerError)
			}
			log.Info(rows)
		}
		w.WriteHeader(http.StatusNotFound)
	})

	return mux
}

// BIOTEndpoint creates another webserver just for the BIOT to post data to
func BIOTEndpoint(port int) *mux.Router {
	listener := mux.NewRouter()
	listener.HandleFunc("/dev/f1", Post).Methods("POST").Name("biot")
	return listener
}
