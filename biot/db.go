package biot

import (
	"github.com/alexbyk/panicif"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/jonjonnetworks/packetpeek-oss/base"
)

// Global Database Pools
var DeviceDB *gorm.DB
var DataDB *gorm.DB
var SettingsDB *gorm.DB

var ArchiveDB *gorm.DB

func BIOTInit(devdbname, datdbname, setdbname string) {
	var err error
	DeviceDB, err = gorm.Open("sqlite3", devdbname)
	panicif.Err(err)
	DeviceDB.DB().SetMaxIdleConns(100)
	DeviceDB.DB().SetMaxOpenConns(200)
	DeviceDB.AutoMigrate(&Device{})

	DataDB, err = gorm.Open("sqlite3", datdbname)
	panicif.Err(err)
	DataDB.DB().SetMaxIdleConns(100)
	DataDB.DB().SetMaxOpenConns(200)
	DataDB.AutoMigrate(&Data{})

	SettingsDB, err = gorm.Open("sqlite3", setdbname)
	panicif.Err(err)
	SettingsDB.DB().SetMaxIdleConns(100)
	SettingsDB.DB().SetMaxOpenConns(200)
	SettingsDB.AutoMigrate(&Settings{})

	// Archiver
	if base.ArchiveEnabled {
		ArchiveDB, err := gorm.Open("sqlite3", "archived_"+datdbname)
		panicif.Err(err)
		ArchiveDB.DB().SetMaxIdleConns(100)
		ArchiveDB.DB().SetMaxOpenConns(200)
		ArchiveDB.AutoMigrate(&Data{})
	}
}
