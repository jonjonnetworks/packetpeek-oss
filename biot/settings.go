package biot

import (
	"encoding/json"
	"net/http"

	"github.com/jinzhu/gorm"
)

type Settings struct {
	gorm.Model
	BIOTPort        int
	GraphPoints     int
	DeviceListStyle string
}

func SettingsImport() error {
	// Make sure there are no existing settings
	var s Settings
	SettingsDB.Find(&s)
	if s.BIOTPort != 0 && s.GraphPoints != 0 && s.DeviceListStyle != "" {
		return nil
	}

	biotSettings := Settings{
		BIOTPort:        7875,
		GraphPoints:     75,
		DeviceListStyle: "nickname",
	}

	return SettingsDB.Create(&biotSettings).Error
}

// GetSettings returns a settings object
func GetSettings() (s Settings) {
	SettingsDB.Find(&s)
	return
}

func SettingsRead(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(GetSettings())
}

// SettingUpdate will update biot settings
func SettingUpdate(w http.ResponseWriter, r *http.Request) {
	var settingsUpdate map[string]interface{} // Using map so we can update the boolean field to false
	if err := json.NewDecoder(r.Body).Decode(&settingsUpdate); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(`{ "message": "Error Unmarshaling JSON" }`))
		return
	}

	var settings Settings
	if SettingsDB.First(&settings).Update(settingsUpdate).RowsAffected < 1 {
		respJSON := []byte(`{ "message": "Settings not found!" }`)
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	w.WriteHeader(http.StatusOK)
}
