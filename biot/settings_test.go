package biot

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSettingsImport(t *testing.T) {
	assert := assert.New(t)
	QuickInit()
	assert.NoError(SettingsImport())
	QuickTeardown(t)
}

func TestSettingRead(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(Routes())
	QuickInit()
	SettingsImport()

	req, err := http.Get(httpServer.URL + "/biot/settings")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	resp := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&resp)
	assert.NoError(err)
	assert.Equal(float64(7875), resp["BIOTPort"])
	assert.Equal(float64(75), resp["GraphPoints"])
	assert.Equal("nickname", resp["DeviceListStyle"])

	QuickTeardown(t)
}

func TestSettingUpdate(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(Routes())
	QuickInit()
	SettingsImport()

	set := map[string]interface{}{"BIOTPort": 7878}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/biot/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusOK, resp.StatusCode)

	req, err := http.Get(httpServer.URL + "/biot/settings")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	response := make(map[string]interface{})
	decoder := json.NewDecoder(req.Body)
	err = decoder.Decode(&response)
	assert.NoError(err)
	assert.Equal(float64(7878), response["BIOTPort"])

	QuickTeardown(t)
}

func TestSettingUpdateFailsOnNoRowsAffected(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(Routes())
	QuickInit()
	SettingsImport()

	set := map[string]interface{}{"oranges": false}
	data2Json, err := json.Marshal(set)
	assert.NoError(err)

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/biot/settings", bytes.NewBuffer(data2Json))
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	QuickTeardown(t)
}

func TestSettingUpdateFailsOnBadInput(t *testing.T) {
	assert := assert.New(t)

	httpServer := httptest.NewServer(Routes())

	req2, err := http.NewRequest("PATCH", httpServer.URL+"/biot/settings", nil)
	assert.NoError(err)

	client := &http.Client{}
	resp, err := client.Do(req2)
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, resp.StatusCode)
}
